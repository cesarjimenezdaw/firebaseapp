package com.example.firebaseapp

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.firebase.auth.FirebaseAuth


class LoQueSeVeSiEstoFunciona : ComponentActivity() {
    private lateinit var firebaseAuth: FirebaseAuth

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()

        setContent {
            MyAppTheme {
                MostrarActivityContent()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    fun MostrarActivityContent() {
        val currentUser = firebaseAuth.currentUser
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {

                Text(
                    text = "Tunombre:, ${currentUser?.displayName ?: "Usuario"}!"
                )
                Text(
                    text = "¿Qué quieres ver?",
                    style = androidx.compose.ui.text.TextStyle(fontSize = 26.sp, color = Color.Magenta),
                    modifier = Modifier.padding(16.dp)
                )
                Button(
                    onClick = {
                        // Ir a la página de navegación
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.marca.com/"))
                        startActivity(intent)
                    },
                    modifier = Modifier.padding(16.dp)
                ) {
                    Text("Ver el furgol")
                }

                Button(
                    onClick = {
                        // Ir a la página de música
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.marca.com/"))
                        startActivity(intent)
                    },
                    modifier = Modifier.padding(16.dp)
                ) {
                    Text("Ver mas furgol")
                }
            }
        }
    }

    @Composable
    fun MyAppTheme(content: @Composable () -> Unit) {
        MaterialTheme(content = content)
    }

}

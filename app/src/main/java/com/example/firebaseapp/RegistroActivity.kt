package com.example.firebaseapp

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegistroActivity : ComponentActivity() {
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()

        setContent {
            MyApplicationPruebaLoginTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Registro()
                }
            }
        }
    }


    @Composable
    fun Registro() {
        val emailState = remember { mutableStateOf("") }
        val passwordState = remember { mutableStateOf("") }

        Column(
            modifier = Modifier.fillMaxSize(),
        ) {
            TextField(
                value = emailState.value,
                onValueChange = { emailState.value = it },
                label = { Text(text = "Email") }
            )
            TextField(
                value = passwordState.value,
                onValueChange = { passwordState.value = it },
                label = { Text(text = "Password") }
            )
            Button(
                onClick = {
                    registro(emailState.value, passwordState.value)
                }
            ) {
                Text(text = "Registrar")
            }
        }
    }

    fun registro(email: String, pass: String) {
        if(email.isNotEmpty() && pass.isNotEmpty() ){
            //damos de alta email y contraseña
            firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this){
                //obtener el id de usuario, sabemos que no es nulo ya que lo acabamos de crear
                val user: FirebaseUser = firebaseAuth.currentUser!!
                verifyEmail(user)
                //tdo ha salido bien, por lo tanto vamos a mostrar
                startActivity(Intent(this@RegistroActivity, LoQueSeVeSiEstoFunciona::class.java))

            }.addOnFailureListener {
                Toast.makeText(this, "Error de autenticación", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Inserta los datos bien", Toast.LENGTH_SHORT).show()
        }
    }

    fun verifyEmail(user: FirebaseUser){
        user.sendEmailVerification().addOnCompleteListener(this){
                task-> if(task.isSuccessful){
            Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
        }
        }
    }

    @Composable
    fun MyApplicationPruebaLoginTheme(content: @Composable () -> Unit) {
        MaterialTheme(content = content)
    }
}
